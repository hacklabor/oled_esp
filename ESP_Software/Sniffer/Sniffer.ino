// ESP8266 Simple sniffer
// 2018 Carve Systems LLC
// Angel Suarez-B Martin




#include <Arduino.h>
#include <ESP8266WiFi.h>
#include "sdk_structs.h"
#include "ieee80211_structs.h"
#include "string_utils.h"
#include <Adafruit_NeoPixel.h>
#include <U8g2lib.h>

#ifdef U8X8_HAVE_HW_SPI
#include <SPI.h>
#endif
#ifdef U8X8_HAVE_HW_I2C
#include <Wire.h>
#endif
#include <U8x8lib.h>
#define CH_TIME 140        /* Scan time (in ms) per channel */
#define LED D5              /* LED pin */
#define LED_NUM 2          /* Number of LEDs */


struct AP_const
{
	uint8_t SenderAddr[6]; /* sender address */
	char ssid1[32] = { 0 };
};


AP_const AP[31];

int APcounter = 0;

const short channels[] = { 1,2,3,4,5,6,7,8,9,10,11,12,13/*,14*/ };
int ch_index { 0 };               // Current index of channel array
int packet_rate { 0 };            // Deauth packet counter (resets with each update)
int attack_counter { 0 };         // Attack counter
unsigned long update_time { 0 };  // Last update time
unsigned long ch_time { 0 };      // Last channel hop time
unsigned long Display_time{ 0 };      // Last Display time
char ssid[32] = { 0 };
int display_KnownSSID = 0;
char attack_addr1[] = "00:00:00:00:00:00\0";
char TxMacAddr[] = "00:00:00:00:00:00\0";
uint8_t attack_SSID_Index = 0;
char buf[40];
int displayChangeTime = 2000;

extern "C"
{
  #include "user_interface.h"
}


Adafruit_NeoPixel pixels{ 1, D5 , NEO_RGB + NEO_KHZ800 }; // Neopixel LEDs
U8G2_SSD1306_128X64_NONAME_F_HW_I2C u8g2(U8G2_R2, /* reset=*/ U8X8_PIN_NONE, /* clock=*/ D2, /* data=*/ D1);   // ESP32 Thing, HW I2C with pin remapping
// According to the SDK documentation, the packet type can be inferred from the
// size of the buffer. We are ignoring this information and parsing the type-subtype
// from the packet header itself. Still, this is here for reference.
wifi_promiscuous_pkt_type_t packet_type_parser(uint16_t len)
{
  	switch(len)
    {
      // If only rx_ctrl is returned, this is an unsupported packet
      case sizeof(wifi_pkt_rx_ctrl_t):
      return WIFI_PKT_MISC;

      // Management packet
      case sizeof(wifi_pkt_mgmt_t):
      return WIFI_PKT_MGMT;

      // Data packet
      default:
      return WIFI_PKT_DATA;
    }
}

// In this example, the packet handler function does all the parsing and output work.
// This is NOT ideal.
void wifi_sniffer_packet_handler(uint8_t *buff, uint16_t len)
{
  // First layer: type cast the received buffer into our generic SDK structure
  const wifi_promiscuous_pkt_t *ppkt = (wifi_promiscuous_pkt_t *)buff;
  // Second layer: define pointer to where the actual 802.11 packet is within the structure
  const wifi_ieee80211_packet_t *ipkt = (wifi_ieee80211_packet_t *)ppkt->payload;
  // Third layer: define pointers to the 802.11 packet header and payload
  const wifi_ieee80211_mac_hdr_t *hdr = &ipkt->hdr;
  const uint8_t *data = ipkt->payload;

  // Pointer to the frame control section within the packet header
  const wifi_header_frame_control_t *frame_ctrl = (wifi_header_frame_control_t *)&hdr->frame_ctrl;

  // Parse MAC addresses contained in packet header into human-readable strings
  char addr1[] = "00:00:00:00:00:00\0";
  char addr2[] = "00:00:00:00:00:00\0";
  char addr3[] = "00:00:00:00:00:00\0";

  mac2str(hdr->addr1, addr1);
  mac2str(hdr->addr2, addr2);
  mac2str(hdr->addr3, addr3);
  
  // Output info to serial
 // Serial.printf("\n%s | %s | %s | %u | %02d | %u | %u(%-2u) | %-28s | %u | %u | %u | %u | %u | %u | %u | %u | ",
 /*   addr1,
    addr2,
    addr3,
    wifi_get_channel(),
    ppkt->rx_ctrl.rssi,
    frame_ctrl->protocol,
    frame_ctrl->type,
    frame_ctrl->subtype,
    wifi_pkt_type2str((wifi_promiscuous_pkt_type_t)frame_ctrl->type, (wifi_mgmt_subtypes_t)frame_ctrl->subtype),
    frame_ctrl->to_ds,
    frame_ctrl->from_ds,
    frame_ctrl->more_frag,
    frame_ctrl->retry,
    frame_ctrl->pwr_mgmt,
    frame_ctrl->more_data,
    frame_ctrl->wep,
    frame_ctrl->strict;*/
  if (frame_ctrl->type == WIFI_PKT_MGMT && (frame_ctrl->subtype == DISASSOCIATION || frame_ctrl->subtype == DEAUTHENTICATION))
  {
	 attack_counter++;
	 for (int i = 0; i < 30; i++)
	 {
		 if (MAC_vergleich(hdr->addr2,AP[i].SenderAddr))
		 {
			 attack_SSID_Index=i;
			 break;
		 }
		 attack_SSID_Index = 30;
	 }

	 mac2str(hdr->addr3, TxMacAddr);
	

	 Serial.print(attack_counter);
  }
  // Print ESSID if beacon
  if (frame_ctrl->type == WIFI_PKT_MGMT && frame_ctrl->subtype == BEACON)

  {
	 
    const wifi_mgmt_beacon_t *beacon_frame = (wifi_mgmt_beacon_t*) ipkt->payload;
    char ssid[32] = {0};

    if (beacon_frame->tag_length >= 32)
    {
      strncpy(ssid, beacon_frame->ssid, 31);
    }
    else
    {
      strncpy(ssid, beacon_frame->ssid, beacon_frame->tag_length);
    }

    //Serial.printf("%s", ssid);
	APcounter = AP_DATA(APcounter, ssid, hdr->addr2);
	
  }

  
}


int AP_DATA(int counter, char ssid_neu[32],const uint8_t adr[6])
{
	for (int z = 0; z < (30); z++)
	{
		if (MAC_vergleich(adr, AP[z].SenderAddr)) { return counter; }
	}
	strcpy(AP[counter].ssid1, ssid_neu);
	
	for (int i = 0; i < 6; i++)


	{
		AP[counter].SenderAddr[i] = adr[i];

	}
	counter++;
	if (counter > 29) { counter = 0; }
	return counter;

}

boolean MAC_vergleich(const uint8_t adr1[6], uint8_t adr2[6])
{
	for (int i = 0; i < 6; i++)
	{
		if (adr1[i] != adr2[i]) { return false; }

	}
	return true;
}


void setup()
{
  // Serial setup
	
	pixels.begin();
	pixels.setPixelColor(0, pixels.Color(5, 5, 5));
	pixels.show();
	
	
	
		u8g2.begin();
		u8g2.clearBuffer();
		u8g2.setFont(u8g2_font_ncenB08_tr);	// choose a suitable font
		u8g2.drawStr(20, 50, "Waiting WLAN Attack");	// write something to the internal memory
		u8g2.sendBuffer();					// transfer internal memory to the display
  Serial.begin(115200);
  delay(10);
  wifi_set_channel(9);

  // Wifi setup
  wifi_set_opmode(STATION_MODE);
  wifi_promiscuous_enable(0);
  WiFi.disconnect();

  // Set sniffer callback
  wifi_set_promiscuous_rx_cb(wifi_sniffer_packet_handler);
  wifi_promiscuous_enable(1);

  // Print header
  //Serial.printf("\n\n     MAC Address 1|      MAC Address 2|      MAC Address 3| Ch| RSSI| Pr| T(S)  |           Frame type         |TDS|FDS| MF|RTR|PWR| MD|ENC|STR|   SSID");
 
}




void loop()
{
	if (attack_counter > 10)
	{

	
		u8g2.clearBuffer();					// clear the internal memory
		u8g2.setFont(u8g2_font_helvB10_tr);
		u8g2.drawStr(0, 15, "Attack");// write something to the internal memory
		u8g2.drawStr(0, 35, "SSID");// write something to the internal memory
		u8g2.drawStr(0, 55, AP[attack_SSID_Index].ssid1);	// write something to the internal memory
		u8g2.sendBuffer();					// transfer internal memory to the display
		Serial.println("Angreifer ");
		Serial.print(TxMacAddr);
		Serial.println("Opfer SSID ");
		Serial.print(AP[attack_SSID_Index].ssid1);
		Serial.println("");
		attack_counter = 0;
		pixels.setPixelColor(0, pixels.Color(255, 0, 0));
		pixels.show();
		delay(50);
		pixels.setPixelColor(0, pixels.Color(0, 0, 255));
		pixels.show();
		delay(50);
		pixels.setPixelColor(0, pixels.Color(255, 0, 0));
		pixels.show();
		delay(50);
		pixels.setPixelColor(0, pixels.Color(0, 0, 255));
		pixels.show();
		delay(50);
		
		displayChangeTime = 10000;
		Display_time = millis();
	}

   unsigned long current_time = millis(); // Get current time (in ms)
   if (sizeof(channels) > 1 && current_time - ch_time >= CH_TIME) {
	   ch_time = current_time; // Update time variable

	   // Get next channel
	   ch_index = (ch_index + 1) % (sizeof(channels) / sizeof(channels[0]));
	   short ch = channels[ch_index];

	   // Set channel
	   //Serial.print("Set channel to ");
	   //Serial.println(ch);
	   wifi_set_channel(ch);
   }
   if (current_time-Display_time>displayChangeTime)
   {
	   displayChangeTime = 2000;
	   pixels.setPixelColor(0, pixels.Color(5, 5, 5));
	   pixels.show();
	   Display_time = current_time;
	for (int i = 0; i < 30; i++) {
		

		char addr1[] = "00:00:00:00:00:00\0";
		mac2str(AP[i].SenderAddr, addr1);
		Serial.print(i);
		Serial.print(".  ");
		Serial.print(addr1);
		Serial.print("    ");
		Serial.println(AP[i].ssid1);

		


	}
	char addr4[] = "00:00:00:00:00:00\0";
	char StrBuf[20];
	boolean vorhanden = false;
	for (int i = 0; i < 6; i++)
		
	{
		if (AP[display_KnownSSID].SenderAddr[i] != 0) {
			vorhanden = true;
			break;
		}
		
	}
	if (!vorhanden) { display_KnownSSID = 0; }
	mac2str(AP[display_KnownSSID].SenderAddr, addr4);
	u8g2.clearBuffer();					// clear the internal memory
	//u8g2.setFont(u8g2_font_ncenB08_tr);	// choose a suitable font
	u8g2.setFont(u8g2_font_helvB10_tr);
	sprintf(StrBuf, "Nr: %d", display_KnownSSID);
	u8g2.drawStr(0 , 15, StrBuf);// write something to the internal memory
	u8g2.drawStr(0, 35, AP[display_KnownSSID].ssid1);// write something to the internal memory
	
	u8g2.drawStr(0, 55, addr4);
	u8g2.sendBuffer();
	display_KnownSSID++;
	if (display_KnownSSID > 29) { display_KnownSSID = 0; }
  }
 delay(10);
}
