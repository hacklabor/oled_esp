




/*



  Universal 8bit Graphics Library (https://github.com/olikraus/u8g2/)

  Copyright (c) 2016, olikraus@gmail.com
  All rights reserved.

  Redistribution and use in source and binary forms, with or without modification,
  are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice, this list
  of conditions and the following disclaimer.

  * Redistributions in binary form must reproduce the above copyright notice, this
  list of conditions and the following disclaimer in the documentation and/or other
  materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
  CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
#include <Arduino.h>
#ifdef U8X8_HAVE_HW_SPI
#include <SPI.h>
#endif

#ifdef U8X8_HAVE_HW_I2C
#include <Wire.h>
#endif

#include <FS.h>
#include <ArduinoJson.h>
#include <ESP8266HTTPClient.h>


#include <WiFiManager.h>

#include <U8g2lib.h>
#include "secrets.h"

const String url_currentWeather = "http://api.openweathermap.org/data/2.5/weather?q=";
const String url_forecastWeather = "http://api.openweathermap.org/data/2.5/forecast?q=";
String City = "Schwerin";
String Country ="DE";
String key ="&appid=";
const char* CONFIG_FILE = "/config.json";
bool readConfigFile();
bool writeConfigFile();

String url_OWM_APIcall_curent = url_currentWeather + City+","+Country + key+ APIKEY1;
String url_OWM_APIcall_forecast = url_forecastWeather + City + "," + Country + key + APIKEY1;


const uint8_t st_bitmap_player1[] =
{
  B00011000,
  B00111100,
  B01111110,
  B11111111,
  B00000000,
  B00000000
};
const uint8_t st_bitmap_player12[] =
{
  B00011000,
  B00111100,
  B01111110,
  B11111111,
  B01000010,
  B01000010
};

String wetterdaten [][3]=
{
{"200","Gewitter mit leichtem Regen","1"},
{"201","Gewitter mit Regen","1"},
{"202","Gewitter mit starkem Regen","1"},
{"210","Leichtes Gewitter","1"},
{"211","Gewitter","1"},
{"212","Schweres Gewitter","1"},
{"221","zerkl�ftetes Gewitter","1"},
{"230","Gewitter mit leichtem Nieselregen","1"},
{"231","Gewitter mit Nieselregen","1"},
{"232","Gewitter mit starkem Nieselregen","1"},
{"300","leichter Nieselregen","2"},
{"301","Nieselregen","2"},
{"302","Starker, intensiver Nieselregen","2"},
{"310","Lichtintensit�t Nieselregen","2"},
{"311","Nieselregen","2"},
{"312","Starker, intensiver Nieselregen","2"},
{"313","Regen und Nieselregen","2"},
{"314","Starker Regen und Nieselregen","2"},
{"321","Nieselschauer","2"},
{"500","Leichter Regen","2"},
{"501","M��iger Regen","2"},
{"502","Starker Regen","2"},
{"503","Sehr starker Regen","2"},
{"504","Extremer Regen","2"},
{"511","gefrierender Regen","3"},
{"520","leichter Regenschauer","2"},
{"521","Regenschauer","2"},
{"522","Starker Regenschauer","2"},
{"531","zeitweise Regenschauer","2"},
{"600","leichter Schneefall","4"},
{"601","Schneefall","Schnee"},
{"602","starker Schneefall","Schnee"},
{"611","Schneeregen","3"},
{"612","Schneeregenschauer","3"},
{"615","leichter Regen und Schneefall","3"},
{"616","Regen mit Schnee","3"},
{"620","leichter Regen und Schneeschauer ","3"},
{"621","Schneeschauer","4"},
{"622","Starke Schneeschauer","4"},
{"701","Nebel","9"},
{"711","Rauch","9"},
{"721","Dunst","9"},
{"731","Sand, Staub wirbelt","9"},
{"741","Nebel","9"},
{"751","Sand","9"},
{"761","Staub","9"},
{"762","Vulkanasche","9"},
{"771","B�en","9"},
{"781","Tornado","9"},
{"800","Klarer Himmel","5"},
{"801","kaum Wolken","6"},
{"802","zeitweise Wolken","6"},
{"803","zeitweise Aufheiterungen","7"},
{"804","geschlossene Wolkendecke","7"},
{"900","Tornado","9"},
{"901","Tropischer Sturm","9"},
{"902","Hurrikan","9"},
{"903","Kalt","9"},
{"904","Hei�","9"},
{"905","Windig","9"},
{"906","Hagel","9"},
{"951","ruhig","5"},
{"952","Leichte Brise","9"},
{"953","Schwache Brise","9"},
{"954","M��ige Brise","9"},
{"955","Frische Brise","9"},
{"956","Starke Brise","9"},
{"957","Starker Wind, fast Sturm","9"},
{"958","Sturm","9"},
{"959","Starker Sturm","9"},
{"960","GewitterSturm","1"},
{"961","Gewitter Gewaltiger Sturm","1"},
{"962","Hurrikan","9"}
};




U8G2_SSD1306_64X48_ER_F_HW_I2C u8g2(U8G2_R2, /* reset=*/ U8X8_PIN_NONE);

const uint8_t Display_Width = 64;
const uint8_t Display_High = 48;
unsigned long LastWeatherData = millis();
unsigned long interval = 0;

//Zeit Variablen
unsigned int Stunde;
unsigned int Minute;
unsigned int Sekunde;
unsigned long Unix_TS;
unsigned long secsSince1900;
unsigned long Unix_TS_sync;
unsigned long Unix_TS_now;
unsigned long Unix_TS_start;
unsigned long start_ms;
// Merker Zeitpunkt der UNIX TS Aktualisierung Wichtig f�r �berlauf der LONG Millis() nach ca 50 Tagen
unsigned long millis_old;
// Zeitpunkt Millis() der letzten NTC Aktualisierung berechnen der Aktuellen Unix Zeit (Unix_TS_now=
unsigned long TimeOffset;
char buf[100];

unsigned int localPort = 8888;       // local port to listen for UDP packets
char timeServer[] = "ptbtime1.ptb.de"; // time.nist.gov NTP server
const int NTP_PACKET_SIZE = 48; // NTP time stamp is in the first 48 bytes of the message
byte packetBuffer[NTP_PACKET_SIZE]; //buffer to hold incoming and outgoing packets
WiFiUDP Udp;						// A UDP instance to let us send and receive packets over UDP


const int BtLeft = D7;
const int BtRight = D6;
const uint8_t PIN_LED = D4;
uint8_t menue = 1;

double temperatur = 273.25;
int wetterId = 0;
double windSpeed = 0;
double windDegree = 0;
int tempPressure = 0;
int tempHumidity = 0;
int tempSichtweite = 0;
int tempWolkendecke = 0;
long dateTime;

String TimeZone = "2";

uint8_t ICON = 127;
String WetterText = "noch keine Daten empfangen";
uint16_t icon1 = 127;
uint16_t icon2 = 127;


void setup(void) {
	Serial.begin(115200);
	pinMode(BtLeft, INPUT_PULLUP);
	pinMode(BtRight, INPUT_PULLUP);
	//pinMode(PIN_LED, OUTPUT);


	u8g2.begin();
	bool result = SPIFFS.begin();
	delay(10);
	Serial.println("SPIFFS opened: " + result);

	if (!readConfigFile()) {
		Serial.println("Failed to read configuration file, using default values");
	}

		
	 url_OWM_APIcall_curent = url_currentWeather + City + "," + Country + key + APIKEY1;
	 url_OWM_APIcall_forecast = url_forecastWeather + City + "," + Country + key + APIKEY1;
	 Serial.println(url_OWM_APIcall_curent);
	 Serial.println(url_OWM_APIcall_forecast);

	u8g2.clearBuffer();         // clear the internal memory
	u8g2.setFont(u8g2_font_ncenB12_tr); // choose a suitable font
	u8g2.drawStr(0, 15, "HAL lo");
	u8g2.drawStr(0, 35, "Wissen");
	u8g2.sendBuffer();          // transfer internal memory to the display
	// Mount the filesystem
	
	
	delay(2000);
	Serial.println(WiFi.localIP());
		
	Udp.begin(localPort);
}

void loop(void) {
	u8g2.clearBuffer();         // clear the internal memory
	// choose a suitable font
	u8g2.setFont(u8g2_font_open_iconic_all_2x_t);
	u8g2.drawGlyph(8, Display_High / 2 + 8, 259);
	u8g2.drawGlyph(Display_Width - 31 + 8, Display_High / 2 + 8, 112);
	u8g2.setFont(u8g2_font_5x8_mr);
	u8g2.drawStr(3, 7, "hacklabor.de");
	u8g2.drawStr(2, Display_High, "Wetter");
	u8g2.drawStr(37, Display_High, "Spiel");
	u8g2.drawFrame(0, 48 / 2 - 14, 32, 28);
	u8g2.drawFrame(32, 48 / 2 - 14, 32, 28);
	u8g2.sendBuffer();
	if (!digitalRead(BtRight)) {
		game();
		while (!digitalRead(BtRight) || !digitalRead(BtLeft))
		{
			delay(10);
		}
	}
	Serial.println(digitalRead(BtLeft));
	if (!digitalRead(BtLeft)) {
		Serial.println("wetter");
		
		

		wetter();
		while (!digitalRead(BtRight) || !digitalRead(BtLeft))
		{
			delay(10);
		}

	}

	// transfer internal memory to the display
	delay(200);
}

#pragma region Wetter
void wetter(void) {
	uint8_t Textnr = 0;
	String Name = "";
	while (!digitalRead(BtRight) || !digitalRead(BtLeft))
	{
		delay(10);
	}

	while (digitalRead(BtRight) || digitalRead(BtLeft))
	{
		
		unsigned long StartTime = millis();

		while (!digitalRead(BtLeft) || !digitalRead(BtRight))
		{
			config(StartTime);
			delay(1);
		}
		
		u8g2.clearBuffer();
		if ((millis() - LastWeatherData)<0){LastWeatherData = millis();}

		if (millis() - LastWeatherData > interval ) {
			GetNtc();
			interval = 300000;
			LastWeatherData = millis();
			HTTPClient http;
			Serial.println("[HTTP] begin...");
			http.begin(url_OWM_APIcall_curent);
			Serial.println("[HTTP] GET...");
			int httpCode = http.GET();
			if (httpCode > 0) {
				Serial.printf("[HTTP] GET... code: %d\n", httpCode);
				if (httpCode == HTTP_CODE_OK) {
					String response = http.getString();
					Serial.println(response);
					StaticJsonBuffer<2000> jsonBuffer;
					JsonObject& root = jsonBuffer.parseObject(response);
					if (!root.success()) {
						Serial.println("Parsen nicht erfolgreich");
						interval = 10000;
					}
					else {
						temperatur = root["main"]["temp"];
						temperatur -= 273.25;
						wetterId = root["weather"][0]["id"];
						windSpeed = root["wind"]["speed"];
						windDegree = root["wind"]["deg"];
						tempPressure = root["main"]["pressure"];
						tempHumidity = root["main"]["humidity"];
						tempWolkendecke = root["clouds"]["all"];
						tempSichtweite = root["visibility"];
						dateTime = root["dt"];
						char str1[32];
						strcpy(str1, root["name"]);
					 Name =String (str1);
					
						Serial.println(temperatur);
						Serial.println(wetterId);
						Serial.println(windSpeed);
						Serial.println(windDegree);
						Serial.println(tempPressure);
						Serial.println(tempHumidity);
						Serial.println(dateTime);
						Serial.println(Name);
						for (int i = 0; i < sizeof(wetterdaten); i++) {
							if (wetterdaten[i][0].toInt() == wetterId) {
								ICON = wetterdaten[i][2].toInt();
								WetterText = wetterdaten[i][1];
								break;

							}

						}

					}
					//openWeatherMap = parseCurrentJson(response);
					//showData();
				}
			}
			else {
				Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
			}
			http.end();

		}
		
		u8g2.clearBuffer();

		// *******************ICON**************

		icon1und2(ICON);
		drawWeatherSymbol(0, 32, icon1);
		Serial.println(icon1);
		Serial.println(icon2);
		
		u8g2.setFont(u8g2_font_courB10_tf);
		
		
		// ***************Temperatur***********
			   
		//runden
		double aktTemp = temperatur;
		if (aktTemp < 0) { aktTemp -= 0.5; }
		if (aktTemp >= 0) { aktTemp += 0.5; }
		int Grad = int(aktTemp);
		//anzeigen
		sprintf(buf, "%+3d",Grad);
		u8g2.drawStr(30, 15, buf);
		u8g2.drawCircle(64 - 3, 5, 2);

		// *****************Wind**************

		u8g2.setFont(u8g2_font_t0_11b_tf);
		//windSpeed = 40;
		sprintf(buf, "%2d", WindstaerkeBft(windSpeed));
		u8g2.drawStr(52,30, buf);
		//windDegree = 90;
		String str = Windrichtung(windDegree);
		str.toCharArray(buf, 3);
		Serial.println(str.length());
		if (str.length()==1) { u8g2.drawStr(37+5, 30, buf); }
		if (str.length() == 2) { u8g2.drawStr(37, 30, buf); }

		// ************Anzeigetext*************
		TimeNow();
		Textnr++;
		if (WetterText == "noch keine Daten empfangen") { Textnr=2; }
		String AnzeigeText = "";
		 if (Textnr==1){ AnzeigeText = Name +" "+Zeit_to_string(); }
		 if (Textnr == 2) { AnzeigeText = WetterText; }
		 if (Textnr == 3) { AnzeigeText = "Wolkendecke: " + String(tempWolkendecke) + " %  "; }
		 if (Textnr == 4) { AnzeigeText = "Sichtweite: " + String(tempSichtweite) + " m  "; }
		 if (Textnr == 5) { AnzeigeText = "Luftdruck: "+ String(tempPressure)+" mBar  "; }
		 if (Textnr == 6) { AnzeigeText = "Luftfeuchte: " + String(tempHumidity) + " % ";}
		 if (Textnr == 7) { AnzeigeText = "Wind: " + String(windSpeed) + "m/s " + String(int(windDegree))+"Grad   "; }
		 if (Textnr == 8) {Textnr=0; }

		
		
		int siz = AnzeigeText.length();
		Serial.println(siz);
		AnzeigeText.toCharArray(buf, siz+1);
		
			Serial.println(AnzeigeText);
		Serial.println(buf);
		
		draw(buf, icon1);
		u8g2.sendBuffer();
		delay(10);
	}
	u8g2.clearBuffer();
	u8g2.sendBuffer();
	
}

uint8_t WindstaerkeBft(double ms)
{
	/*                            m/s      m/s
	0	Windstille, Flaute	        0 � < 0, 3
		1	leiser Zug	         0, 3 � < 1, 6
		2	leichte Brise 	     1, 6 � < 3, 4
		3	schwache Brise       3, 4 � < 5, 5
		4	m��ige Brise	     5, 5 � < 8, 0	
		5	frische Brise	     8, 0 � < 10, 8	
		6	starker Wind	    10, 8 � < 13, 9
		7	steifer Wind	    13, 9 � < 17, 2
		8	st�rmischer Wind    17, 2 � < 20, 8	
		9	Sturm	            20, 8 � < 24, 5
		10	schwerer Sturm	    24, 5 � < 28, 5
		11	orkanartiger Sturm	28, 5 � < 32, 7	
		12	Orkan	             ? 32, 7	
	*/
	uint8_t bft = 0;
	if (ms > 0.3) { bft = 1;}
	if (ms > 1.6) { bft = 2; }
	if (ms > 3.4) { bft = 3; }
	if (ms > 5.5) { bft = 4; }
	if (ms > 8.0) { bft = 5; }
	if (ms > 10.8) { bft = 6; }
	if (ms > 13.9) { bft = 7; }
	if (ms > 17.2) { bft = 8; }
	if (ms > 20.8) { bft = 9; }
	if (ms > 24.5) { bft = 10; }
	if (ms > 28.5) { bft = 11; }
	if (ms > 32.7) { bft = 12; }
	return bft;
}

String Windrichtung(double degree) {
	String Windrichtung = "N";
	if (degree >= 22.5) { Windrichtung = "NO";}
	if (degree >= 45+22.5) { Windrichtung = "O";}
	if (degree >= 90+22.5) { Windrichtung = "SO";}
	if (degree >= 90+45+22.5) { Windrichtung = "S";}
	if (degree >= 180+22.5) { Windrichtung = "SW";}
	if (degree >= 180+45.0+22.5) { Windrichtung = "W";}
	if (degree >= 270+22.5) { Windrichtung = "NW";}
	if (degree >= 270 + 45.0 + 22.5) {Windrichtung = "N";}
	return Windrichtung;


}

void draw(const char* s, uint16_t symbol)
{
	int16_t offset = (int16_t)u8g2.getDisplayWidth();
	int16_t len = strlen(s);

					// clear the internal memory
	
	if (WetterText == "noch keine Daten empfangen") { u8g2.clearBuffer();  }
	
	
	for (;;)							// then do the scrolling
	{
		if (!digitalRead(BtRight) || !digitalRead(BtLeft)) { break; }
		
		u8g2.setDrawColor(0);		// clear the scrolling area
		u8g2.drawBox(0, 49, u8g2.getDisplayWidth() - 1, u8g2.getDisplayHeight() - 1);
		u8g2.setDrawColor(1);		// set the color for the text
		u8g2.setFont(u8g2_font_8x13_mf);
		u8g2.drawStr(offset , 46, s);
		u8g2.sendBuffer();				// transfer internal memory to the display

		delay(10);
		
		offset --;
		if (offset < len*-8 )
			break;
	}
}
void icon1und2(uint8_t symbol) {
	/*
		1 170 Gewitter
		2 241 Regen und Nieselregen
		3 241/258 Regen mix mit Schnee
		4 258 Schnee
		5 259/223 Sonnig/Klare Nacht
		6 127 teils bew�lkt
		7 124 bew�lkt
	*/

	switch (symbol)

	{
	case 1:
		icon1 = 170;
		icon2 = 170;
		break;
	case 2:
		icon1 = 241;
		icon2 = 241;
		break;
	case 3:
		icon1 = 241;
		icon2 = 258;
		break;
	case 4:
		icon1 = 258;
		icon2 = 258;
		break;
	case 5:
		icon1 = 259;
		icon2 = 223;
		break;
	case 6:
		icon1 = 127;
		icon2 = 127;
		break;
	case 7:
		icon1 = 124;
		icon2 = 124;
		break;

	}
}




void drawWeatherSymbol(u8g2_uint_t x, u8g2_uint_t y, uint16_t symbol)
{
	// fonts used:
	// u8g2_font_open_iconic_embedded_6x_t
	// u8g2_font_open_iconic_weather_6x_t
	// encoding values, see: https://github.com/olikraus/u8g2/wiki/fntgrpiconic


	/*
		1 170 Gewitter
		2 241 Regen
		3 258 Schnee
		4 259 Sonnig
		5 223 klare nacht
		6 127 teils bew�klk
		7 124 bew�lkt
		*/
	Serial.println(symbol);
	u8g2.setFont(u8g2_font_open_iconic_all_4x_t);
	u8g2.drawGlyph(x, y, symbol);
		
}
#pragma endregion




#pragma region GAME
void game(void) {

	while (1) {
		uint8_t  takt;
		uint8_t loch[48];
		uint8_t loch_pos[48];
		uint8_t level = 1;
		uint64_t score = 0;
		uint64_t nextLVLscore = 1000;
		boolean gameOver = false;
		boolean leftdrall = false;
		uint8_t x_sp = Display_Width / 2 - 2;
		uint8_t Displaytime = 120;
		u8g2.setFont(u8g2_font_5x8_mr);

		for (int i = 0; i < 48; i++) {
			loch[i] = Display_Width - 3;
			loch_pos[i] = 1;
		}

		while (!gameOver) {
			takt++;
			if (!digitalRead(BtLeft)) {
				x_sp--;
			}
			if (!digitalRead(BtRight)) {
				x_sp++;
			}
			u8g2.clearBuffer();

			//lines eine nach unten kopieren
			for (int i = 1; i < 48; i++) {
				loch[48 - i] = loch[47 - i];
				loch_pos[48 - i] = loch_pos[47 - i];
			}
			uint16_t randomloch = random(13 - level);

			if (randomloch == 0) {
				loch[0]++;
				if (loch[0] > Display_Width - 2) {
					loch[0] = Display_Width - 2;
				}
				if (loch[0] > 35 - level) {
					loch[0] --;
				}
				if (loch[0] + loch_pos[0] > Display_Width - 2) {
					loch[0]--;
				}

			}

			if (randomloch == 1) {
				loch[0]--;
				if (loch[0] < 25 - level) {
					loch[0] ++;
				}
			}
			uint16_t randomlochpos = random(15 - level);

			if ((randomlochpos == 0)) {
				loch_pos[0]++;

				if (loch[0] + loch_pos[0] > Display_Width - 2) {
					loch_pos[0]--;
				}
				if (loch_pos[0] < 35 || leftdrall == false) {
					if (loch_pos[0] == 34) { leftdrall == true; }
					loch_pos[0]++;

					if (loch[0] + loch_pos[0] > Display_Width - 2) {
						loch_pos[0]--;
					}
				}

			}

			if ((randomlochpos == 1)) {
				if (loch_pos[0] > 5 || leftdrall == true) {
					if (loch_pos[0] == 6) { leftdrall == false; }
					loch_pos[0]--;
				}
				loch_pos[0]--;
				if (loch_pos[0] < 1) {
					loch_pos[0] = 1;
				}
			}
			if (Displaytime > 0) {


				Displaytime--;
				loch_pos[0] = 20 + level;
				loch[0] = 24 - level;
				if (Displaytime > 60) {
					sprintf(buf, "Level %d", level);
					u8g2.drawStr(10, 30, buf);
					loch_pos[0] = 1;
					loch[0] = Display_Width - 3;
				}

			}
			for (int i = 0; i < 48; i++) {
				u8g2.drawLine(0, i, loch_pos[i] - 1, i);
				u8g2.drawLine(loch_pos[i] + loch[i] + 1, i, Display_Width, i);

			}
			if (loch_pos[42] > x_sp + 3 || loch_pos[42] + loch[42] < x_sp + 5) {
				gameOver = true;
				u8g2.drawStr(10, 30, "GAME OVER");

			}
			if (loch_pos[43] > x_sp + 2 || loch_pos[43] + loch[43] < x_sp + 6) {
				gameOver = true;
				u8g2.drawStr(10, 30, "GAME OVER");
			}
			if (loch_pos[44] > x_sp + 1 || loch_pos[44] + loch[44] < x_sp + 7) {
				gameOver = true;
				u8g2.drawStr(10, 30, "GAME OVER");
			}
			if (loch_pos[45] > x_sp || loch_pos[45] + loch[45] < x_sp + 8) {
				gameOver = true;
				u8g2.drawStr(10, 30, "GAME OVER");
			}

			if ((takt & 2) == 2) {
				u8g2.drawBitmap(x_sp, Display_High - 6, 1, 6, st_bitmap_player1);
				//Serial.println("TRUST");
			}
			else
			{
				u8g2.drawBitmap(x_sp, Display_High - 6, 1, 6, st_bitmap_player12);
				//Serial.println("NoTRUST");
			}
			score += level;

			sprintf(buf, "%d", score);
			u8g2.drawStr(0, 9, buf);
			if (score > nextLVLscore) {
				level++;
				if (level > 10) {
					level--;
					gameOver = true;
					u8g2.drawStr(10, 30, "YOU WIN");
				}
				nextLVLscore = score + (1000 * level);
				Displaytime = 120;
			}



			u8g2.sendBuffer();
			delay(15 - level);
		}
		u8g2.setFont(u8g2_font_open_iconic_all_2x_t);
		u8g2.drawGlyph(0, Display_High, 121);

		u8g2.drawGlyph(Display_Width - 16, Display_High, 120);
		u8g2.sendBuffer();

		delay(500);
		while (digitalRead(BtLeft) && digitalRead(BtRight)) {
			delay(10);
		}
		if (!digitalRead(BtLeft)) {
			return;
		}
	}

}

#pragma endregion

#pragma region configFile

void config(unsigned long startzeit) {
	
	u8g2.setDrawColor(0);		// clear the scrolling area
	u8g2.drawBox(0, 49, u8g2.getDisplayWidth() - 1, u8g2.getDisplayHeight() - 1);
	u8g2.setDrawColor(1);		// set the color for the text
	u8g2.setFont(u8g2_font_8x13_mf);
	uint8_t rest = 3-((millis() - startzeit) / 1000);
	Serial.println(rest);
	sprintf(buf, "config:%d", rest);
	u8g2.drawStr(0, 46, buf);
	u8g2.sendBuffer();				


	

	if (millis() - startzeit < 3000){return; }


		Serial.println(String(ESP.getChipId()).c_str());

		u8g2.clearBuffer();         // clear the internal memory
		u8g2.setFont(u8g2_font_5x8_mr); // choose a suitable font
		uint8_t Zeile = 9;
		u8g2.drawStr(0, 9, "WLAN AP");

		Zeile += 9;
		u8g2.drawStr(0, Zeile, "ESP");
		u8g2.drawStr(15, Zeile, String(ESP.getChipId()).c_str());
		Zeile += 9;
		u8g2.drawStr(0, Zeile, "192.168.4.1");
		Zeile += 9;
		u8g2.drawStr(0, Zeile, "zur Config");
		u8g2.sendBuffer();                // the Wifi radio's status






		Serial.println("Configuration portal requested");
		digitalWrite(PIN_LED, LOW); // turn the LED on by making the voltage LOW to tell us we are in configuration mode.
	   //Local intialization. Once its business is done, there is no need to keep it around

	   // Extra parameters to be configured
	   // After connecting, parameter.getValue() will get you the configured value
	   // Format: <ID> <Placeholder text> <default value> <length> <custom HTML> <label placement>

	   // Thingspeak API Key - this is a straight forward string parameter
		APIKEY1.toCharArray(buf, 33);
		WiFiManagerParameter p_ApiKey("apikey", "OWA Api Key", buf, 33);
		City.toCharArray(buf, sizeof City + 2);
		WiFiManagerParameter p_City("city", "Stadt ", buf, sizeof City + 2);
		Country.toCharArray(buf, sizeof Country + 2);
		WiFiManagerParameter p_Country("country", "Land K�rzel ", buf, sizeof Country + 2);
		TimeZone.toCharArray(buf, 5);
		WiFiManagerParameter p_tz("tz", "Zeit", buf,5);


		WiFiManagerParameter p_hint("<small>*Hint: if you want to reuse the currently active WiFi credentials, leave SSID and Password fields empty</small>");

		// Initialize WiFIManager
		WiFiManager wifiManager;

		//add all parameters here

		wifiManager.addParameter(&p_hint);
		wifiManager.addParameter(&p_ApiKey);
		wifiManager.addParameter(&p_City);
		wifiManager.addParameter(&p_Country);
	wifiManager.addParameter(&p_tz);


		// Sets timeout in seconds until configuration portal gets turned off.
		// If not specified device will remain in configuration mode until
		// switched off via webserver or device is restarted.
		// wifiManager.setConfigPortalTimeout(600);

		// It starts an access point 
		// and goes into a blocking loop awaiting configuration.
		// Once the user leaves the portal with the exit button
		// processing will continue
		if (!wifiManager.startConfigPortal()) {
			Serial.println("Not connected to WiFi but continuing anyway.");
		}
		else {
			// If you get here you have connected to the WiFi
			Serial.println("Connected...yeey :)");
		}
		APIKEY1 = p_ApiKey.getValue();
		City = p_City.getValue();
		Country = p_Country.getValue();
		TimeZone = p_tz.getValue();
		// Writing JSON config file to flash for next boot
		writeConfigFile();
		digitalWrite(PIN_LED, HIGH); // turn the LED on by making the voltage LOW to tell us we are in configuration mode.


		ESP.reset(); // This is a bit crude. For some unknown reason webserver can only be started once per boot up 
		// so resetting the device allows to go back into config mode again when it reboots.
		delay(5000);
		Serial.println("CONFIG ENDE");
	
}

bool readConfigFile() {
	// this opens the config file in read-mode
	File f = SPIFFS.open(CONFIG_FILE, "r");

	if (!f) {
		Serial.println("Configuration file not found");
		return false;
	}
	else {
		// we could open the file
		size_t size = f.size();
		// Allocate a buffer to store contents of the file.
		std::unique_ptr<char[]> buf9(new char[size]);

		// Read and store file contents in buf
		f.readBytes(buf9.get(), size);
		// Closing file
		f.close();
		// Using dynamic JSON buffer which is not the recommended memory model, but anyway
		// See https://github.com/bblanchon/ArduinoJson/wiki/Memory%20model
		DynamicJsonBuffer jsonBuffer;
		// Parse JSON string
		JsonObject& json = jsonBuffer.parseObject(buf9.get());
		// Test if parsing succeeds.
		if (!json.success()) {
			Serial.println("JSON parseObject() failed");
			return false;
		}
		json.printTo(Serial);

		// Parse all config file parameters, override 
		// local config variables with parsed values
		if (json.containsKey("owmApiKey")) {
			char str1[32];
			strcpy(str1, json["owmApiKey"]);
			String str(str1);
			APIKEY1 = str;

		}
		if (json.containsKey("owmCity")) {
			char str1[32];
			strcpy(str1, json["owmCity"]);
			String str(str1);
		City = str;
		}
		if (json.containsKey("owmCountry")) {
			char str1[32];
			strcpy(str1, json["owmCountry"]);
			String str(str1);
		Country = str;
		}
		if (json.containsKey("TimeZone")) {
			char str1[32];
			strcpy(str1, json["TimeZone"]);
			String str(str1);
			TimeZone = str;
		}

		
	}
	Serial.println("\nConfig file was successfully parsed");
	return true;
}

bool writeConfigFile() {
	Serial.println("Saving config file");
	DynamicJsonBuffer jsonBuffer;
	JsonObject& json = jsonBuffer.createObject();

	// JSONify local configuration parameters
	json["owmApiKey"] = APIKEY1;
	json["owmCity"]=	City ;
	json["owmCounty"]= Country ;
	json["TimeZone"] = TimeZone;
	// Open file for writing
	File f = SPIFFS.open(CONFIG_FILE, "w");
	if (!f) {
		Serial.println("Failed to open config file for writing");
		return false;
	}

	json.prettyPrintTo(Serial);
	// Write data to file and close it
	json.printTo(f);
	f.close();

	Serial.println("\nConfig file was successfully saved");
	return true;
}

#pragma endregion

#pragma region NTP Zeitserver
//*****************Begin NTC Timserver Funktionen************************
void sendNTPpacket(char* address) {
	// set all bytes in the buffer to 0
	memset(packetBuffer, 0, NTP_PACKET_SIZE);
	// Initialize values needed to form NTP request
	// (see URL above for details on the packets)
	packetBuffer[0] = 0b11100011;   // LI, Version, Mode
	packetBuffer[1] = 0;     // Stratum, or type of clock
	packetBuffer[2] = 6;     // Polling Interval
	packetBuffer[3] = 0xEC;  // Peer Clock Precision
							 // 8 bytes of zero for Root Delay & Root Dispersion
	packetBuffer[12] = 49;
	packetBuffer[13] = 0x4E;
	packetBuffer[14] = 49;
	packetBuffer[15] = 52;

	// all NTP fields have been given values, now
	// you can send a packet requesting a timestamp:
	Udp.beginPacket(address, 123); //NTP requests are to port 123
	Udp.write(packetBuffer, NTP_PACKET_SIZE);
	Udp.endPacket();
}



void GetNtc() {
	sendNTPpacket(timeServer); // send an NTP packet to a time server

							   // wait to see if a reply is available
	delay(1000);
	if (Udp.parsePacket()) {
		// We've received a packet, read the data from it
		Udp.read(packetBuffer, NTP_PACKET_SIZE); // read the packet into the buffer

												 // the timestamp starts at byte 40 of the received packet and is four bytes,
												 // or two words, long. First, extract the two words:

		unsigned long highWord = word(packetBuffer[40], packetBuffer[41]);
		unsigned long lowWord = word(packetBuffer[42], packetBuffer[43]);
		// combine the four bytes (two words) into a long integer
		// this is NTP time (seconds since Jan 1 1900):
		secsSince1900 = highWord << 16 | lowWord;
		Serial.print("Seconds since Jan 1 1900 = ");
		Serial.println(secsSince1900);

		// now convert NTP time into everyday time:
		Serial.print("Unix time = ");
		// Unix time starts on Jan 1 1970. In seconds, that's 2208988800:
		const unsigned long seventyYears = 2208988800UL;
	
		start_ms = millis();
		TimeOffset = 0;
		// subtract seventy years:
		unsigned long epoch = secsSince1900 - seventyYears;

		Unix_TS_sync = epoch;
		// print Unix time:
		Serial.println(epoch);
		
	
		// print the hour, minute and second:
		Serial.print("The UTC time is ");       // UTC is the time at Greenwich Meridian (GMT)
		Serial.print((epoch % 86400L) / 3600); // print the hour (86400 equals secs per day)
		Serial.print(':');
		if (((epoch % 3600) / 60) < 10) {
			// In the first 10 minutes of each hour, we'll want a leading '0'
			Serial.print('0');
		}
		Serial.print((epoch % 3600) / 60); // print the minute (3600 equals secs per minute)
		Serial.print(':');
		if ((epoch % 60) < 10) {
			// In the first 10 seconds of each minute, we'll want a leading '0'
			Serial.print('0');
		}
		Serial.println(epoch % 60); // print the second
	}
	// wait ten seconds before asking for the time again
}
//*****************ENDE NTC Timserver Funktionen************************
//*****************Begin ZEIT Funktionen************************
void ZeitUnix(unsigned long UnixT) {

	Serial.println("TIMEUNIX");



	Stunde = (UnixT % 86400L) / 3600;
	Minute = (UnixT % 3600) / 60;
	Sekunde = UnixT % 60;
	Serial.println(Stunde);
	Serial.println(Minute);
	Serial.println(Sekunde);


}

void TimeNow() {
	// �berlauf Millis bei 4294967296ms Abfangen
	if (millis() < millis_old) {
		TimeOffset += 4294967;
	}
	Serial.println("TIMENOW");
	int tmp = TimeZone.toInt() * 3600;
	Unix_TS_now = (Unix_TS_sync + TimeOffset + (millis() / 1000) - (start_ms / 1000))+tmp  ;
	millis_old = millis();
	ZeitUnix(Unix_TS_now);
}



void ZeitMillis(unsigned long Time) {
	//Zerlegen Unix Timestamp in Stunde, Minute und Sekunde
	Time = Time / 1000;
	Serial.println(Time);



	Stunde = (Time / 3600);
	Minute = (Time % 3600) / 60;
	Sekunde = Time % 60;
	Serial.println(Stunde);
	Serial.println(Minute);
	Serial.println(Sekunde);

}

String Zeit_to_string() {
	// Formatierung der Zeitausgabe mit f�hrenden Nullen
	String Zeit = "";
	if (Stunde < 10) { Zeit += "0"; }
	Zeit += String(Stunde) + ":";

	if (Minute < 10) { Zeit += "0"; }
	Zeit += String(Minute) ;
	return Zeit;
}



// calculates the daylight saving time for middle Europe. Input: Unixtime in UTC (!)

	
//*****************Ende ZEIT Funktionen************************

#pragma endregion

