// Visual Micro is in vMicro>General>Tutorial Mode
// 
/*
    Name:       main.ino
    Created:	04.02.2019 19:33:31
    Author:     DESKTOP-4QEDG4C\Thk
*/

// Define User Types below here or use a .h file
//


// Define Function Prototypes that use User Types below here or use a .h file
//


// Define Functions below here or use other .ino or cpp files
//

// This software is licensed under the MIT License.
// See the license file for details.
// For more details visit github.com/spacehuhn/DeauthDetector

// include necessary libraries


#include <Wire.h>
#include <SPI.h>
#include <U8x8lib.h>
#include <ESP8266WiFi.h>       // For the WiFi Sniffer
#include <Adafruit_NeoPixel.h> // For the Neopixel/WS2812 LED(s)

// include ESP8266 Non-OS SDK functions
extern "C" {
#include "user_interface.h"
}

// ===== SETTINGS ===== //
#define LED D5              /* LED pin */
#define LED_NUM 1          /* Number of LEDs */
#define SERIAL_BAUD 115200 /* Baudrate for serial communication */
#define CH_TIME 140        /* Scan time (in ms) per channel */
#define PKT_RATE 5         /* Min. packets before it gets recognized as an attack */
#define PKT_TIME 1         /* Min. interval (CH_TIME*CH_RANGE) before it gets recognized as an attack */

// Channels to scan on (US=1-11, EU=1-13, JAP=1-14)
const short channels[] = { 1,2,3,4,5,6,7,8,9,10,11,12,13/*,14*/ };
short ch ;
// ===== Runtime variables ===== //
Adafruit_NeoPixel pixels { LED_NUM, LED, NEO_RGB + NEO_KHZ800 }; // Neopixel LEDs
int ch_index { 0 };               // Current index of channel array
int packet_rate { 0 };            // Deauth packet counter (resets with each update)
int attack_counter { 0 };         // Attack counter
unsigned long update_time { 0 };  // Last update time
unsigned long ch_time { 0 };      // Last channel hop time
char ssid[32] = { 0 };
// ===== Sniffer function ===== //
void sniffer(uint8_t *buf, uint16_t len) {
  if (!buf || len < 28) return; // Drop packets without MAC header
  for (int i = 0; i < len; ++i) {
	 // Serial.print(buf[i]);
	  //Serial.print(";");
	 
  }
  
  byte pkt_type = buf[12]; // second half of frame control field
  //byte* addr_a = &buf[16]; // first MAC address
  //byte* addr_b = &buf[22]; // second MAC address

  // If captured packet is a deauthentication or dissassociaten frame
  


  if (pkt_type == 0xA0 || pkt_type == 0xC0) {
	  char addr1[] = "00:00:00:00:00:00\0";
	  sprintf(addr1, "%02x:%02x:%02x:%02x:%02x:%02x", buf[16], buf[17], buf[18], buf[19], buf[20], buf[21]);
	  Serial.print("MAC1: ");
	  Serial.println(addr1);
	  char addr2[] = "00:00:00:00:00:00\0";
	  sprintf(addr2, "%02x:%02x:%02x:%02x:%02x:%02x", buf[22], buf[23], buf[24], buf[25], buf[26], buf[27]);
	  Serial.print("MAC2: ");
	  Serial.println(addr2);
    ++packet_rate;
	Serial.print("Channel: ");
	Serial.println(ch);

  }
}

// ===== Attack detection functions ===== //
void attack_started() {
  for(int i=0; i<LED_NUM; ++i)
    pixels.setPixelColor(i, pixels.Color(255,0,0));
  pixels.show();
  Serial.println("ATTACK DETECTED");
}

void attack_stopped() {
  for(int i=0; i<LED_NUM; ++i)
    pixels.setPixelColor(i, pixels.Color(0,255,0));
  pixels.show();
  Serial.println("ATTACK STOPPED");
}

// ===== Setup ===== //
void setup() {
  Serial.begin(SERIAL_BAUD); // Start serial communication

  // Init LEDs
  pixels.begin();
  for(int i=0; i<LED_NUM; ++i)
    pixels.setPixelColor(i, pixels.Color(0,5,0));
  pixels.show();

  WiFi.disconnect();                   // Disconnect from any saved or active WiFi connections
  wifi_set_opmode(STATION_MODE);       // Set device to client/station mode
  wifi_set_promiscuous_rx_cb(sniffer); // Set sniffer function
  wifi_set_channel(channels[0]);        // Set channel
  wifi_promiscuous_enable(true);       // Enable sniffer

  Serial.println("Started \\o/");
}

// ===== Loop ===== //
void loop() {
  unsigned long current_time = millis(); // Get current time (in ms)

  // Update each second (or scan-time-per-channel * channel-range)
  if (current_time - update_time >= (sizeof(channels)*CH_TIME)) {
    update_time = current_time; // Update time variable

    // When detected deauth packets exceed the minimum allowed number
    if (packet_rate >= PKT_RATE) {
      ++attack_counter; // Increment attack counter
    } else {
      if(attack_counter >= PKT_TIME) attack_stopped();
      attack_counter = 0; // Reset attack counter
    }

    // When attack exceeds minimum allowed time
    if (attack_counter == PKT_TIME) {
      attack_started();
    }

    Serial.print("Packets/s: ");
    Serial.println(packet_rate);

    packet_rate = 0; // Reset packet rate
  }

  // Channel hopping
  if (sizeof(channels) > 1 && current_time - ch_time >= CH_TIME) {
    ch_time = current_time; // Update time variable

    // Get next channel
    ch_index = (ch_index+1) % (sizeof(channels)/sizeof(channels[0]));
	ch = channels[ch_index];

    // Set channel
    //Serial.print("Set channel to ");
    //Serial.println(ch);
    wifi_set_channel(ch);
  }

}
